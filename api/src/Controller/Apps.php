<?php

// Change "MyApp" to the name of your app //
namespace MyApp\Controller;

use FW\Manager;
use FW\Router\Standard\AbstractController;

class Apps extends AbstractController {

    public function getApps() {
        $manager = new Manager();
        $programs = $this->request->get('ids');
        $programs = explode(',', $programs);
        $toReturn = array();
        foreach ($programs as $program) {
            $programInfo = $manager->getAppInfo($program);
            if ($programInfo != null) {
                $toReturn[] = $programInfo;
            }
        }
        $this->view->helper('json')->add($toReturn)->render();
    }
}
