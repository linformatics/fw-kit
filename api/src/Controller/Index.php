<?php

// Change "MyApp" to the name of your app //
namespace MyApp\Controller;

use FW\Router\Standard\AbstractController;

class Index extends AbstractController {

    public function helloAction() {
        echo 'Welcome to FW Kit';
    }
}
