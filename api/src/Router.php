<?php

// Edit this with the name of your app //
namespace MyApp;

use FW\Router\Standard\AbstractRouteMapper;

/**
 * This class maps api requests (e.g. api/v1.php/users/find)
 * to specific classes (called 'controllers') and specific methods
 * in those classes (called 'actions')
 */
class Router extends AbstractRouteMapper {

    /**
     * This is the array of controllers for your application
     *
     * This array should be in the form of a key => value pair,
     * where the key is the controller alias, and the value is
     * the actual controller class name.
     *
     * If you want the Router to ensure your controllers are
     * going to work with the Router module, controllers need
     * to go in this array. Otherwise, you can just pass the
     * full classname into the map() function
     *
     * @var array
     */
    protected $controllers = array(
        // This is a generic index controller, and can be removed
        'index' => 'Index',
        // This is a built-in controller to load other applications in FW.
        // If you wish your application to interact with other applications,
        // leave this controller here
        'apps' => 'Apps'
    );

    /**
     * This function maps specific routes to controller actions
     * The method is passed a Slim application instance, which is
     * use to create specific routes and groups
     *
     * For further documentation on how to create routes,
     * consult the Slim Framework documentation
     * (http://docs.slimframework.com)
     *
     * @param  \Slim\Slim $app The Slim application
     */
    public function mapRoutes(\Slim\Slim $app) {
        // These variables are assigned because PHP 5.3 does not support
        // the $this variable in anonymous function context
        $c = $this->controllers;
        $map = $this;

        // Note: To load a controller (e.g. inject dependencies and
        // return an object instance of the controller) you must call
        // a particular function.
        // To load the controller instance, call $map->load()
        // The load() function accepts one argument, which is the
        // name of the controller class. If you are using controller
        // aliasing (e.g. the $controllers array up top), pass in the
        // value from the $c array based on the alias key
        // Once you have loaded the controller instance,
        // create the routes

        // The 'use' keyword is necessary to pass those variables into
        // the function's scope
        $app->group('/', function () use ($app, $c, $map) {
            $index = $map->load($c['index']);
            $app->get('hello(/)', array($index, 'helloAction'));
        });

        // this isn't in a group, so no need for the $app->group()
        // function call
        $apps = $map->load($c['apps']);
        $app->get('/apps(/)', array($apps, 'getApps'));
    }
}
