import DS from 'ember-data';
import Ember from 'ember';

var ApplicationAdapter = DS.RESTAdapter.extend({
    config: Ember.inject.service(),

    host: Ember.computed(function () {
        return this.get('config.apiRoot');
    }),

    // TODO: Figure out what the effects of these two methods are
    shouldReloadAll: function () {
        return true;
    },

    shouldBackgroundReloadRecord: function () {
        return false;
    }
});

export default ApplicationAdapter;
