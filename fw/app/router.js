import Ember from 'ember';
import envConfig from './config/environment';

var Router = Ember.Router.extend({
    location: envConfig.locationType,
    rootURL: null,

    config: Ember.inject.service(),

    init: function () {
        this.set('rootURL', this.get('config.baseURL'));
        this._super.apply(this, arguments);
    }
});
// DON'T EDIT ABOVE THIS LINE! //

// Add your routes here
export default Router.map(function () {

});
