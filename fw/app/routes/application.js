import Ember from 'ember';
import ModalActionMixin from 'fw-ember/mixins/modal-action';

export default Ember.Route.extend(ModalActionMixin, {
    config: Ember.inject.service(),

    actions: {
        reload: function () {
            window.location.replace(this.get('config.baseURL'));
        }
    }
});
