/* global require, module */
var EmberApp = require('ember-cli/lib/broccoli/ember-app');

module.exports = function(defaults) {
    var app = new EmberApp(defaults, {
        lessOptions: {
            paths: ['bower_components/fw-style/less']
        },
        hinting: false
    });

    // Stop: Normalize
    app.import('bower_components/normalize-css/normalize.css');

    // FontAwesome CSS
    app.import('bower_components/font-awesome/css/font-awesome.css');

    // FontAwesome Fonts
    app.import('bower_components/font-awesome/fonts/fontawesome-webfont.eot', {destDir: 'fonts'});
    app.import('bower_components/font-awesome/fonts/fontawesome-webfont.svg', {destDir: 'fonts'});
    app.import('bower_components/font-awesome/fonts/fontawesome-webfont.ttf', {destDir: 'fonts'});
    app.import('bower_components/font-awesome/fonts/fontawesome-webfont.woff', {destDir: 'fonts'});
    app.import('bower_components/font-awesome/fonts/fontawesome-webfont.woff2', {destDir: 'fonts'});
    app.import('bower_components/font-awesome/fonts/FontAwesome.otf', {destDir: 'fonts'});

    return app.toTree();
};
