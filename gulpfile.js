/* global require, process */
/*
 * FW Kit Gulpfile v1.0
 *
 * Contains tasks necessary to setup and build the application.
 *
 * Sections:
 *
 * 1. Setup/Help Tasks
 * 2. Build/Copy Tasks
 * 3. Clean Tasks
 * 4. Testing/Validation Tasks
 * 5. Release Compilation Tasks
 * 6. Miscellaneous/Temporary Tasks
 *
 */
var _        = require('lodash'),
    fs       = require('fs'),
    del      = require('del'),
    zip      = require('gulp-zip'),
    path     = require('path'),
    jscs     = require('gulp-jscs'),
    file     = require('gulp-file'),
    chalk    = require('chalk'),
    phpcs    = require('gulp-phpcs'),
    slash    = require('slash'),
    shell    = require('gulp-shell'),
    gulpif   = require('gulp-if'),
    jshint   = require('gulp-jshint'),
    rename   = require('gulp-rename'),
    phplint  = require('phplint').lint,
    phpunit  = require('gulp-phpunit'),
    replace  = require('gulp-replace'),
    sequence = require('run-sequence'),
    gulp = require('gulp-help')(require('gulp'), {
        aliases: ['h'],
        hideEmpty: true,
        hideDepsMessage: true
    }),

    pkg = require('./package.json'),
    config_defaults = {
        apiPath: '../../',
        phpVersion: 5.3,
        outputDirectory: './dist/',
        url: '/',
        host: 'http://example.com:80/'
    },
    config = _.assign(config_defaults, require('./fw.json')),

    apiRoot = slash(path.resolve(config.apiPath)),
    appPath = slash(path.resolve('./fw')),
    jsPath = './fw/app/**/*.js',
    shellOpts = {
        cwd: appPath,
        env: {
            PATH: [process.env.PATH, appPath + '/node_modules/.bin'].join(':')
        }
    },
    cleanPaths = {
        built: [
            'fw/dist/**',
            'fw/tmp/**',
            '.fw.json'
        ],
        dependencies: [
            'fw/node_modules/**',
            'fw/bower_components/**',
            'node_modules/**'
        ]
    };

config.apiVersion = config.apiVersion || JSON.parse(fs.readFileSync(apiRoot + '/.fw-cli')).version;

/**
 * Builds a shell command
 * @param  {Object} options
 * @return {string} the command as a string
 */
function buildCommand(options) {
    var command;

    options = options || {};
    command = options.bin || '';

    if (options.cmd) {
        command += ' '+options.cmd;
    }

    if (options.args) {
        _.forEach(options.args, function (n, key) {
            command += ' -'+key+'"'+n+'"';
        });
    }

    return command;
}

/*
 * 1. Setup/Help Tasks
 */

gulp.task('init', 'Installs ember-cli npm and bower dependencies', shell.task(['npm install', 'bower install'], {cwd: slash(path.resolve('fw/'))}));

gulp.task('migrate', 'Migrates or rolls back the database', shell.task([
    buildCommand({
        bin: 'php ' + apiRoot + '/vendor/bin/phinx',
        cmd: (process.argv[3] === '--down') ? 'rollback' : 'migrate',
        args: {
            c: './api/config/phinx.php',
            e: 'default'
        }
    })
]));

/*
 * 2. Build/Copy Tasks
 */

gulp.task('dev', 'Builds the Ember app in the development environment', function () {
    sequence('dist:clean', 'fw:build', 'ember:build', ['dist:copy', 'api:copy']);
});

gulp.task('prod', 'Builds the Ember app in the production environment', function () {
    sequence('dist:clean', 'fw:build', 'ember:build', ['dist:copy', 'api:copy']);
});

gulp.task('watch', 'Runs the Ember app in watch mode at http://localhost:4200',['api:copy', 'fw:build'], shell.task(['ember serve --proxy ' + config.host + ' --watch'], shellOpts));

// Shell task to actually build the ember app
gulp.task('ember:build', shell.task(['ember build --environment=<%= env() %>'], _.assign(shellOpts, {
    templateData: {
        env: function () {
            switch(process.argv[2]) {
                case 'prod':
                    return 'production';
                case 'dev':
                    return 'fwdev';
                default:
                    return 'fwdev';
            }
        }
    },
})));

// Task to copy the files from the ember dist directory to the actual production directory
gulp.task('dist:copy', function (cb) {
    console.log(chalk.green('Copying files from fw/dist....'));
    gulp.src('./fw/dist/**/*')
        .pipe(gulp.dest(config.outputDirectory));
    cb();
});

// Copies the api calling file to the output directory
gulp.task('api:copy', function (cb) {
    var file = 'v' + config.apiVersion  + '.php';

    console.log(chalk.green('Copying api files....'));
    gulp.src('./api/' + file)
        .pipe(replace('{{api-path}}', apiRoot))
        .pipe(replace('{{app-code}}', pkg.shortcode))
        .pipe(gulp.dest(config.outputDirectory + 'api/'));
    cb();
});

gulp.task('fw:build', function () {
    var output = {
        name: pkg.name.replace(/-/, ' '),
        programId: pkg.shortcode,
        version: pkg.version,
        url: config.url,
        apiVersion: config.apiVersion
    };

    _.assign(output, pkg.fw);

    return file('.fw.json', JSON.stringify(output, null, 2), {src: true})
            .pipe(gulp.dest('./'));
});

/*
 * 3. Clean Tasks
 */

gulp.task('clean', 'Removes all built files and installed dependencies', ['dist:clean'], function () {
    console.log(chalk.green('Removing built files....'));
    del.sync(cleanPaths.built);
    console.log(chalk.green('Removing dependencies....'));
    del.sync(cleanPaths.dependencies);
});

// TODO: Fix for windows
gulp.task('dist:clean', function (cb) {
    if(config.outputDirectory === './' || config.outputDirectory === '/') {
        throw new Error(chalk.red('cannot output to root or home directory'));
    }
    console.log(chalk.blue('Removing old dist files....'));
    del(config.outputDirectory + '**/*', {force: true}, cb);
});

/*
 * 4. Testing/Validation Tasks
 */

// 4.1 Linter Tasks
gulp.task('lint', 'Lints all files and checks all files for code style errors',
    ['lint:client', 'lint:server', 'sniff:client', 'sniff:server', 'sniff:compat']);

gulp.task('lint:client', function () {
    return gulp.src(jsPath)
            .pipe(jshint())
            .pipe(jshint.reporter('default'))
            .pipe(jshint.reporter('fail'));
});

gulp.task('lint:server', function (cb) {
    phplint(['./api/src/**/*.php'], {limit: 10}, function (err) {
        if (err) {
            cb(err);
            process.exit(1);
        }
        cb();
    });
});

gulp.task('sniff:client', function () {
    return gulp.src(jsPath)
            .pipe(jscs({
                esnext: true,
                configPath: '.jscsrc'
            }));
});

//phpcs function
function cs(compat) {
    return (function () {
        var bin = apiRoot + '/vendor/bin/phpcs' + (compat ? ' --runtime-set testVersion ' + config.phpVersion : ''),
            standard = (compat) ? 'PHPCompatibility' : apiRoot + '/dev/FW/';

        return gulp.src('./api/src/**/*.php')
                .pipe(phpcs({
                    bin: bin,
                    standard: standard,
                    colors: true
                }))
                .pipe(phpcs.reporter('log'))
                .pipe(phpcs.reporter('fail'));
    });
}

gulp.task('sniff:server', cs(false));

gulp.task('sniff:compat', cs(true));

// 4.2 Testing Tasks
gulp.task('test-ember', shell.task(['ember test'], shellOpts));

gulp.task('test:unit:setup', function () {
    return gulp.src('phpunit.xml')
            .pipe(replace('{{VENDOR_PATH}}', apiRoot + '/vendor/autoload.php'))
            .pipe(rename('.phpunit'))
            .pipe(gulp.dest('./'));
});

gulp.task('test-unit', ['test:unit:setup'], function () {
    gulp.src('.phpunit')
        .pipe(phpunit(apiRoot + '/vendor/bin/phpunit', {debug: false, notify: true, silent: true}));
});

/*
 * 5. Release Compilation Tasks
 */
gulp.task('ember:build:release', shell.task(['ember build -prod -o "./../public/"'], shellOpts));

gulp.task('release', 'Compiles and zips the code for release', ['ember:build:release', 'fw:build'], function () {
    var fileName = pkg.name.toLowerCase() + '-' + pkg.version;
    return gulp.src([
        '!./api/config/local.php',
        '!./api/config/phinx.php',
        '!./api/.*',
        '!./api/tests/**',
        '!./api/tests',
        './api/**/**',
        './public/**/**',
        '.fw.json',
        'install.js'
    ], {base: './'})
        .pipe(gulpif(function (file) {
            return /\.dist$/i.test(file.path);
        }, rename({ extname: '' })))
        .pipe(replace('{{app-code}}', pkg.shortcode))
        .pipe(zip(fileName + '.zip'))
        .pipe(gulp.dest('./'));
});

/*
 * 6. Miscellaneous/Temporary Tasks
 */
